import 'rxjs/add/operator/toPromise';

import {Injectable} from '@angular/core';
import {Http} from '@angular/http';

import {environment} from '../environments/environment';


@Injectable()
export class ApiService {
    url: string = environment.api;

    constructor(private http: Http) {}


    getBands (): Promise<any> {
        return this.http.get(`${this.url}/bands`)
            .toPromise()
            .then(response => response.json());
    }

    // getBand (bandId: string): Promise<any> {}

    // getArtists (bandId: string): Promise<any> {}

    // getAlbums (bandId: string): Promise<any> {}

    // getAlbum (albumId: string): Promise<any> {}

    // getTrack (trackId: string): Promise<any> {}

    // getCommentsForTrack (trackId: string): Promise<any> {}

    // postCommentForTrack (postData: object): Promise<any> {}

}
