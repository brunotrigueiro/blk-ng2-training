import {Component, Input, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {

    @Input() size: number;

    currentPage: number = 1;
    lastPage: number = null;
    pageSize: number = 10;

}
