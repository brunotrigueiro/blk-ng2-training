import {Component, Input } from '@angular/core';


@Component({
  selector: 'vertical-list',
  templateUrl: './vertical-list.component.html',
  styleUrls: ['./vertical-list.component.scss'],
})
export class VerticalListComponent {

    @Input()
    data: Array<any>;

    @Input()
    paginate?: boolean = false;

    @Input()
    type: string;

}
